import "./App.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";
// @ts-ignore
import { CartStoreProvider } from "shell/cart-store";

import { CheckoutPage } from "./CheckoutPage";

function App() {
  return (
    <CartStoreProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<CheckoutPage />} />
        </Routes>
      </BrowserRouter>
    </CartStoreProvider>
  );
}

export default App;
